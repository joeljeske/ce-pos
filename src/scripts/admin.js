function AdminCtrl($scope) { 
	"use strict";

	//Build a list of settings key to get from the DB		
	var rows = ['machine_id', 'location', 'campus', 'invoice_prefix', 'last_sync', 'created', "drawer_code", "cut_code", "live_mode"];
	
	//Get them and apply the return object to the $scope for angular access
	CE.DB.settings.get( rows ).done(function(vals){ 
		$scope.model = vals; 
		$scope.$apply();
	});
	
	$scope.close = function(){
		chrome.app.window.current().close();
	}
	
	$scope.sync =  function sync(isAll) {
		CE.util.loader.start(); 
		CE.POS.sync(isAll).always(CE.util.loader.stop).done($scope.$apply);
	}
	
	$scope.deleteDB = function(){
		CE.util.confirm('Confirm', 'Are you sure you want to delete the entire local database?').done( function(){
			
			CE.util.loader.start();
		
			var promise = CE.DB.reset();
			
			promise.fail(function() { console.log('failed') });
			promise.always(CE.util.loader.stop);
			
			var logoutPromise = promise.then(function() {
				return CE.util.alert('Logout Needed', 'You will be logged out now.');
			});
			
			
			logoutPromise.done(function(){ 
				//Cannot syncronize here due to invalid (lackof) session token
				//When user logs in- he will be loaded with most basic permissions until a second logout
				chrome.app.window.current().close();
				CE.main.logout(); 
			});
		});		
	}
	
	$scope.prettyDate = function(unix_time){
		return new Date(unix_time).toLocaleString();
	}
	
	$scope.save = function() { 
		CE.util.loader.start();
		
		if($scope.model.machine_id)
		{
			CE.POS.configure($scope.model.machine_id)
				.then(function(result) { 
						var settings = {};
						
						if(result && result.response)
						{
							$scope.model.campus = result.response.campus;
							$scope.model.location = result.response.location;						
							$scope.$apply();	
						}
						return CE.DB.settings.put($scope.model);
					},
					function() { 
						CE.log.error('There was a save error when saving configuration settings');
					}
				).always(CE.util.loader.stop);
				
		} 
		else
		{
			CE.DB.settings.put($scope.model).always(CE.util.loader.stop);
		}
	}	
}