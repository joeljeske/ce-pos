(function() { 
	"use strict"

	/* Listeners */
	$(document).ready(function() { 		
		$('#login_content>*').hide();
				
		CE.DB.promise.done( function(){ 
			$('#login_content>*').show(); 
			$('button').click( function(){ login(); } );
			$(document).keypress(function(e) { if(e.keyCode == 13) login(); })		
		});

	});
	
	
	/* function called internally on login click */
	function login() { 
		CE.util.loader.start();
		var un = $('input[name=username]').val() || "joeljeske14@gmail.com";
		var pw = $('input[name=password]').val() || "1Password";
		$('input[name=password]').val('');
		
		var loginPromise = CE.POS.login(un, pw);		
	
		loginPromise.done(function(result) { 
			$('input[name=username]').val('');

			var options = {
				"minHeight" : 700,
				"minWidth": 1100,
				"bounds" : {
					"width": 1200,
					"height": 900
				},
				"state" : "normal" /* "fullscreen" */
			};
		
			chrome.app.window.create('index.html', options, copyCE);
			//CE.window.open('index.html', options, copyCE);
		});
		
		loginPromise.fail(function(result) { 
			CE.util.alert('Login Failed', 'Invalid username or password'); 
		});
		
		loginPromise.always(CE.util.loader.stop );
	}
	
	function copyCE(win) {
		win.contentWindow.CE = CE || {};
		$.extend(CE, win.contentWindow.CE);
		chrome.app.window.current().hide();
		win.onClosed.addListener( function() { chrome.app.window.current().show(); });
	}
})();