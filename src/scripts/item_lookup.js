(function() { 
	"use strict";
	
	$(document).ready(function() {
		/* Listeners */
		$('button#find').click(query);
		$('#name, #sku').keydown(function(e) { if(e.keyCode == 13) query(); });

		$('table#items').hide();
		$('#emptyMessage').hide();		
		$('#name').focus();
		
		initDefault();
	});
	
	function initDefault() { 
		if(typeof loaded_name == "string") //initialized with search name
		{
			$('#name').val( loaded_name );
			query();
		}		
		
		if(typeof loaded_sku == "string") //initialized with search sku
		{
			$('#sku').val( loaded_sku );
			query();
		}		
	}
	
	function selectItem($row) { 
		if($row)
		{
			var sku = $row.attr('data-sku');
			if(sku && sku.length > 0)
			{
				if( typeof apply == "function")
				{
					apply( sku );
					chrome.app.window.current().close();
				}
				else
				{
					throw "Could not locate call back function";
				}
			}
		}
	}
	
	function query() { 
		var sku = $('#sku').val();
		var name = $('#name').val();
		var promise = $.when(searchForIn(sku, 'sku'), searchForIn(name, 'name'));
		promise.done(function(m1, m2) { 

			var inter = CE.util.intersect.objects(m1, m2, 'id');			
			$('#initMessage').hide();
			$('table#items').hide();
			$('#emptyMessage').hide();
			
			if(inter && inter.length > 0)
			{
				buildResults(inter);
				$('table#items').show();
			}
			else if(sku.length > 0 || name.length > 0)
			{
				$('#emptyMessage').show();
			}
			else
			{
				$('#initMessage').show();
			}
			
		});
		promise.fail(function(e) { console.log(e); });
	}
	
	function buildResults(merch) { 
		var res = [];
		for(var i in merch)
		{
			var m = merch[i];

			res.push('<tr data-sku="' + (m.sku || ' ') + '">');
			res.push('<td><button class="discrete" data-role="select">Select</button></td>');
			res.push('<td><span>' + (m.sku || ' ') + '</span></td>');
			res.push('<td class="floating"><span>' + (m.name || ' ') + '</span></td>');
			res.push('<td><span>' + (m.size || 'N/A') + '</span></td>');
			res.push('<td><span>' + ( m.quantity || m.quantity === 0 ? m.quantity : '') + '</span></td>');
			res.push('</tr>');
		}
		
		$('table#items tbody').html( res.join('') );
		
		$('button[data-role=select]').click(function() { selectItem( $(this).parents('tr').first() ); });
		$('.items tbody tr').dblclick(function() { selectItem( $(this) ); });
		
		$('#initMessage').hide();
		$('table#items').show();
		$('#emptyMessage').hide();			
	}
	
	function searchForIn(q,i) {
		if(q && q.length > 0)
			return CE.DB.merch.find[i](q);
		return $.Deferred().resolve();
	}
	
	
})();