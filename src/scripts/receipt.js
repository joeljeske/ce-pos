
function ReceiptCtrl($scope) {
	
	//Create function to return css to hide the element if it should not be displayed
	$scope.hide = function(shouldShow){
		if(!shouldShow){
			return ' display:none; ';
		}
	}
	
	//Was a transaction number correctly passed in	
	if(typeof window.loaded_sale_id != "number")
		throw 'Cannot locate sale id';
		
	//Load the receipt, then print, then close
	loadReceipt( window.loaded_sale_id )
		.then(printReceipt, function(){ CE.log.error("Tried to print a receipt with an invalid transaction number."); })
		.done(function(){ chrome.app.window.current().close(); });	

	
	function loadReceipt(id) {
		var data = ["city", "street", "state", "postal", "campus", "location", "phone"];
		var promise = $.when(CE.DB.settings.get(data), CE.DB.trans.get(id));
		
		return promise.then(function(location, transaction){
				
				//Set the view model in the scope		
				$scope.location = location;
				$scope.transaction = transaction;	
				
				//Show the correct date format
				$scope.transaction.transaction_date = new Date(transaction.transaction_date).toLocaleString();
				
				//Tell Angular JS that we have updated the model
				$scope.$apply();			
				
				var copiesToPrint = 1;
				if(transaction.payment.credit_card)
				{
					copiesToPrint++;
				}
				
				//Return information about what should be printed or cash drawer opened
				return {
					copies: copiesToPrint,
					drawer:	transaction.payment.credit_card != undefined
				};
		});
	}
	
	
	function printReceipt(data){
		//If drawer should be opened. Only if true, not just truthy
		if(data.drawer === true)
			CE.print.openDrawer();
	
		//Initialize copies to 1 if parameter comes in incorrectly 
		if(typeof data.copies !== "number")
			data.copies = 1;
	
		//Delete any elements that are hidden
		//QZ-Print does not correctly hide hidden elements
		$(':hidden').remove();
		
		//Wrap the html correctly
		var toPrint = "<html>";
		toPrint += $('body').html();
		toPrint += "</html>";
		
		/*
		for(var i = 0; i < data.copies; i++)
		{
			CE.print.appendHTML(toPrint);
			CE.print.printHTML();
			CE.print.cutPaper();
		}
		*/
	}
	
		
}
