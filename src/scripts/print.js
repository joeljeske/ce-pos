var CE = (function(ce_){
	"use strict";
	var externalOrigin = "*";
	var ready = false;
	
	//Holds reference to webview tag element
	var print;
	
	//Lets get access to the print applet
	$(document).ready(function(){
		//The webview frame that contains the applet
		print = document.getElementById("print-frame");
		
		
		//When the applet has loaded
		print.addEventListener('contentload', function() {			
			window.setTimeout(function(){ sendCommand("handshake"); }, 2500);
		});
		
	});
	
	window.onmessage = function(event) {
		CE.log.info("Received message from a different window");
		var method, args;
		
		if(event.data.method)
			method = event.data.method;
		if(event.data.args)
			args = event.data.args;
		
		applyMethod(method, args);
	}
	
	
	function applyMethod(method, args){
		var argmnts = args || [];
		
		//Check if really a function
		if(typeof methods[method] === "function")
		{
			//Apply method with arguments
			methods[method].apply(argmnts);
		}
	}
	
	var methods = {
		handshake : function(ack){ if(ack){ ready = true; } }
	}
	

	/*
	 * Internal function to send a command to the printer hosted page
	 */
	function sendCommand(method, args){
		var argumnts = args || [];
		var data = {
			"method": method,
			"args": argumnts
		};
		print.contentWindow.postMessage(data, externalOrigin);
	}
	
	/*
	 * Prints a hex string from a settings key in the DB
	 */
	function printHexFromSettings(settingsKey, immediately){
		//Get the hex code from the settings and convert to the needed hex string and send to the printer
		CE.DB.settings.get(settingsKey).done(function(vals){
			
			//If we have the data
			if(vals[settingsKey])
			{
				//Convert to the appropriate Hex string
				var hex = decimalToHex(vals[settingsKey]);

				CE.log.debug("Appending hex to code to next print: " + hex);
				
				CE.print.appendHex(hex);
				
				if(immediately)
					CE.print.print();
			}
		});
	}
	
	/*
	 * Converts a decimal, comma separated string to an 'x' separated string as needed for QZ-Print
	 */
	function decimalToHex(val){
		//Will hold the hex data
		var hex;
		var hexArray = [];
		var decimalArray = val.split(",");
		
		for(var i in decimalArray)
		{
			//Convert each data piece into hex and store in the other array
			hexArray.push( parseInt(decimalArray[i], 10).toString(16).toUpperCase() );
		}
		
		//Use 'x' as the hex byte seperator
		hex = "x" + hexArray.join("x");
		
		return hex;
	}
	
	
	
	
	ce_.print = {
		"ready" : ready,
		"find"  : function(p){ sendCommand("findPrinter", [p]); },
		"append" : function(val){ sendCommand("append", [val]); },
		"append64" : function(val){ sendCommand("append64", [val]); },
		"appendHex" : function(val){ sendCommand("appendHex", [val]); },
		"appendHTML" : function(html){ sendCommand("appendHTML", [html]); },
		"print" : function(){ sendCommand("print"); },
		"printPS" : function(){ sendCommand("printPS"); },
		"printHTML" : function(){ sendCommand("printHTML"); },
		"openDrawer" : function(immediately){ printHexFromSettings("drawer_code", immediately); },
		"cutPaper" : function(immediately){ printHexFromSettings("cut_code", immediately); },
		"_send" : sendCommand
	}
	
	return ce_;
	
})(CE || {});