# Camp Eagle - Sales

## Overview
This repository contains the necessary files to run a browser-based Point of Sale system on a Chrome Browser in conjunction with web-services designed in SalesForce.com.

## Current Work 
### To implement
2. Implement Receipt printing
	1. Finish lookup and data injection for sales in receipt.js
	2. Find solution for silent printing in chrome web app 
	POSSIBLE: --kiosk --kiosk-printing
	3. Find solution for sending print commands for opening drawer/cutting paper.
	POSSIBLE: qz-print (included in zip)
		
3. Sales button at top of index.html should open a window to display a list of sales
		where the user can then print any of the receipts ( or view them?). 

5. Implement a server side feature on any transaction to set the synchronizationNeeded flag on the response. This is needed to ensure the Camp Account balance accuracy from machine to machine. 

6. Ensure the Authorize.Net Card Present protocol handles returns properly. 
	
### Known Bugs
1. If session is offline and a transaction has been made, when regaining internet access, the system successfully converts the session to an online state but does not continue and with a POS.sync() which would upload the offline transactions.
	
2. When deleting the database entirely, it takes two logins to give proper permissions: one login to download the user database and permissions and another login to apply them. The system assumes no permissions unless granted. THIS COULD BE THE DESIRED AFFECT.
	
3. CSS should make the <content-wrapper> tag be as tall as possible without scrolling unless content enforces the height is larger than the page, thus making it scroll. This affects most pages. 
		
4. After a checkout, the first item added to the cart is incorrectly displayed. (i.e. Unit Price: NaN) This may be due to an invalid state of the 'values' object used in document.js.
		
	
		
	
		
